import argparse
import NGramModel


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--model', help='path to save your model')
    parser.add_argument('--input-dir', help='path to test data, stdin by default')
    args = parser.parse_args()
    model = ''
    if args.input_dir:
        model = NGramModel.NGramModel(args.model, args.input_dir, 2)
    else:
        model = NGramModel.NGramModel(args.model, n=2)
    model.fit()


if __name__ == '__main__':
    main()