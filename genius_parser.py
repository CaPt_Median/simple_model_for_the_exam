# вспомогательная часть, где готовим данные для обучения
# вытаскиваю и немного редачу тексты песен
# добавил консольный ввод, чтобы можно было руками докидывать ещё текстов в датасет
from lyricsgenius import Genius  # это апи не разрешено заданием, но я хотел прикольные данные вытащить для обучения
import json
import codecs
import os
import argparse


def main():
    parser = argparse.ArgumentParser()
    artist = 'Metallica'
    max_songs = 2
    parser.add_argument('--artist', help='name of the artist whose tracks you want to download')
    parser.add_argument('--max-songs', type=int, help='max number of songs you want to download')
    args = parser.parse_args()
    if args.artist:
        artist = args.artist
    if args.max_songs:
        max_songs = args.max_songs

    data = json.load(open('config.json'))  # ну я поверю, что не стоит делиться своим токеном для IP
    token = data['client_token']
    path = data['path_to_music']
    if not (os.path.exists(path)):
        os.mkdir(path)
    genius = Genius(token)
    artist = genius.search_artist(artist, max_songs=max_songs)
    songs = artist.songs
    print(path)
    for song in songs:
        if len(song.lyrics) < 10:
            continue
        title = song.full_title
        title = title[:title.find('by')]
        title = path + title + '.txt'
        if title in os.listdir(path):
            continue
        n = len(title)
        i = len(path)
        while i < n:
            if title[i] in '/\\:*?"<>|+':  # жёстко захардкодил плохие символы в имени файла
                title = title[:i] + title[i + 1:]  # переписать на регулярки
                n -= 1
            i += 1

        text = song.lyrics
        while '[' in text:
            i = text.find('[')
            j = text.find(']')
            text = text[:i] + text[j + 1:]
        j = text.find('Embed')
        i = j - 1
        while text[i].isdigit():
            i -= 1
        text = text[:i + 1]
        j = text.find('Lyrics')
        text = text[j + 6:]
        # print(title)
        file = codecs.open(title, 'w', 'utf-8')
        file.write(text)
        file.close()


if __name__ == '__main__':
    main()
