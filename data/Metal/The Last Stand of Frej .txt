See him rise
From land of flames;
Destruction is at hand
It is time to make a stand
Now I face an awesome foe
I will always stand my ground;
To this end my fate is bound
This fight is mine and mine alone
And there's no help from anyone
His wrath burns!
With insane heat
All his fury is unleashed
There is no way to defeat!
The forces and the power that he wields
My hand holds the horn so firm:
I am calm and ready to die
Everything around me burns
And I know that I will not survive

See him rise
From land of flames
Destruction is at hand
It is time to make a stand
My death awaits, I have no fear
To this end my fate is bound
Though I'm doomed I'll stand my ground

This fight is mine and mine alone
And there's no help from anyone

I go forth to meet my doom
But I will die in vain
Perdition waits for everyone
The world will die in flames
With all my strength
I run the horn
Deep into his eye
And as he swings
His burning sword
I die with a tired smile