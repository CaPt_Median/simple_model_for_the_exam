
I will not play along
With things I find unfair
I will not mount defenses
Against unjust attacks
This land was never given
We built this on our own
It's sanity dependent
Whether we rise or fall


Whether we rise or we fall
One for the night, one for the uncontrolled

Hold your feet to the ground
To the end of our time for the rest of our lives
Hold your head up high
To the end of our time for the rest of our lives


Secretly I hope
That nothing ever comes of this
And you are not alone in wanting to come back
To a place where it won't matter just what side you're on
It's where our lives are branded
What's underneath will overcome


What's underneath will overcome
One for the night, one for the uncontrolled


Hold your feet to the ground
To the end of our time for the rest of our lives
Hold your head up high
To the end of our time for the rest of our lives


Now it's time to stand up tall
One for the night, one for the uncontrolled

Hold your feet to the ground
To the end of our time for the rest of our lives
Hold your head up high
To the end of our time for the rest of our lives