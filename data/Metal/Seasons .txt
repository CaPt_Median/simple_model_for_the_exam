I'm searching through emptiness
And tried to forget us in vain
The light of the dark setting sun
Will bring my sadness to an end

Voices cry out through the fear and the doubt
As we wait for our lives to be better

The words are dying in the night
No winter lasts forever
The seasons pass and sunlight will shine
On my life again
So let the past now burn down in flames
Locked in a prison, in a world of living fear
On the edge of my destruction marching on
Still suffer in darkness with the dreams of life not meant to be
As I lie awake and curse the rising sun

Sadness inside from the truth that she hides
And this pain in my heart for no reason


The words are dying in the night
No winter lasts forever
The seasons pass and sunlight will shine
On my life again
So let the past now burn down in flames

♪ 

Stare into my eyes, I'm burning with lust and desire
Won't you come down and let yourself go in the fire
So leave the past behind

♪ 

The words are dying in the night
No winter lasts forever
The seasons pass and sunlight will shine
On my life again
So leave the past behind
The dark clouds fading from my mind
No pain will last forever
The seasons pass and sunlight will shine on my life again
So let the past now burn down in flames
Now burn down in flames

♪ 