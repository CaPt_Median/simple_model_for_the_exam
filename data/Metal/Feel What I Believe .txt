
Behind the logical within
So far from the lines I've crossed, you'll see
I'll bring an end to this
The fall from something greater
Conflicting thoughts result in chaos
It's a chaos that is mine


If you could only see me now
If only you could know
And feel what I believe
To be real and always true
Be what I claim to be lost
In the emptiness that haunt me still
The helplessness that break me

If you could only see me now
If only you could know
If you could ever understand
And feel what I believe


I feel it coming
I feel it coming to a close
Where promisеs are lies, it sets mе up
For disappointment through and through, where chaos
Claims the straight and narrow


If you could only see me now
If only you could know
If you could ever understand
And feel what I believe


The shift in our perspective
Leave us further from, withdrawn
What causes to this friction
Are we accountable for



If you could only see me now
If only you could know
If you could ever understand
And feel what I believe


If only you could see
If only you would know
If only you could see
And feel what I believe


If only you could see
If only you would know
If only you could see
And feel what I believe