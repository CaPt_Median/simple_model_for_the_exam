
It's wearing on my mind
I'm speaking all my doubts aloud
You rob a dead man's grave
Then flaunt it like you did create


If I hit bottom and everything's gone
In the great Mississippi, please drown me and run


It's digging time again
You're nurturing the weakest trend

Those with the heart and the brain to get past this
Can spot a pathetic without even asking


Fuck your magazine, and
Fuck the long-dead plastic scene
Pierce a new hole
If Hell was "in" you'd give your soul to


The Great Southern Trendkill
That's right
The Great Southern Trendkill, fuck yeah!


Buy it at a store
From MTV to on the floor
You look just like a star
It's proof you don't know who you are


If I hit bottom and everything's gone
In the great Mississippi, please drown me and run


It's bullshit time again
You'll save the world within your trend

Those with the heart and the brain to get past this
Can spot a pathetic without even asking


Politically relieved
Your product sold and well-received
The right words spun in gold
If I was God you'd sell your soul to


The Great Southern Trendkill
That's right
The Great Southern Trendkill, fuck yeah!
Huah!


Let's do this one Southern-style