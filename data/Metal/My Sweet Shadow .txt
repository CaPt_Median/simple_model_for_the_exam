
I'm selling heavenly sketches
A world out of my mind
Ready to explode in purity
To fill the holes inside
An ever moving stream
With glowing rays of light
Emotions tied to pass lies
And I know I should let go


Tamed with confidence of a brighter future
I found a flame in the burnt out ashes
Burn out, burn out

Fueled
These new shores burn
Dark past lies cold
Shadow, my sweet shadow
To you I look no more


Another dawn collapses
Do I need to be reminded?
A glimpse of my safe home
A path to hide all anger


I found a flame in the burnt out ashes
Burn out, burn out


Fueled
These new shores burn
Dark past lies cold
Shadow, my sweet shadow
To you I look no more


In circles I catch
A torch carried by the immortal
From depths that I created
In vain, echoes fade
Burn out

 
Fueled
These new shores burn
Dark past lies cold
Shadow, my sweet shadow
To you I look no more

Fueled
These new shores burn
Dark past lies cold
Shadow, my sweet shadow
To you I look no more


Fueled
Dark past lies cold
For you I look no more