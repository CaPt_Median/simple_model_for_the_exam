
Born the son of a legend
What will be my fate?
Will his shadow always loom above?
Or will my name prevail?
The weight I carry from his past
Has always weighed me down
I've struggled hard to find my path
To prove I am my own


Ironside
That's my name
It will live in infamy
Ironside
Sing my name
For all eternity

I have traveled on the wind
It's brought me far and wide
Through rain of blood and storms of steel
My legacy is mine


Ironside
That's my name
It will live in infamy
Ironside
Sing my name
For all eternity


(Järnsida, Järnsida)
(Järnsida, Järnsida)


And never has my blood been shed
In countless wars and fights
This kenning is my true name
They call me Ironside


Now here I sit on Svitjod's throne
The land that's now my home
And when my soul to Asgård's flown
Here I'll rest my bones

Ironside
That's my name
It will live in infamy
Ironside
Sing my name
For all eternity
Ironside
That's my name
It will live in infamy
Ironside
Sing my name
For all eternity


I'm Ironside
Svitjod's king
I am Svitjod's king