
Every time I'm left alone
My misery begins to drown me
Tied by a rope of anxiety
Thrown overboard


As I'm pulled under the tides
Of this fast-paced world
(Of this fast-paced world)
I refuse to see

Time will always be (be)
The thing that kills me truly (truly)
Open these eyes waking (waking)
From a dream feigning (feigning)


My lungs fill up with letdown
Disappointment in self and everyone
Expectations died in failure
Abandonment, my un-savior


As I'm pulled under the tides
Of this fast-paced world
(Of this fast-paced world)
I refuse to see


Time will always be (be)
The thing that kills me truly (truly)
Open these eyes waking (waking)
From a dream feigning (feigning)


Absolution at the sight of your demise
I know what I must do
Choke
Die choking on your every word
Swallow every blasphemy
Absolution at the sight of your demise
I know what I must do
Swallow every blasphemy
Eat out your heart and make it bleed


Tie the noose around my neck
Make this life end