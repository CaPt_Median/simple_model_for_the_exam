
Kill, fight, die
That's what a soldier should do
Top of their game, earning their name
They were the Devil dogs
In a war machine
They were the USA marines


1918, USA intervene
Until now they were mainly observing
There in the wheat fields and a small piece of land
It's a battle that'll write history
Five times attacked, and then five times repelled
And the sixth time, they managed to break the line
Heart of the corps and a part of the lore
The deadliest weapon on Earth

Kill, fight, die
That's what a soldier should do
Top of their game, earning their name
They were the Devil dogs
In a war machine
They were the USA marines


Dogs lead ahead and attack through the lead
Put to test at the battle of Belleau
Clearing the forest and advance through the trees
It's the end of the war that's in sight
Hill 142, it's a final breakthrough
It's the key to controlling the battlefield
Second to none, a marine and a gun
And the foes run in fear of their name


Kill, fight, die
That's what a soldier should do
Top of their game, earning their name
They were the Devil dogs
In a war machine
They were the USA marines

In times they are needed, such times they appear
When a leader has fallen, a hero arise
And inspire the lost into glorious deeds that
Would give them a name that live on to this day
When in times they are needed, such times they appear
When a leader has fallen, a hero arise
And inspire the lost into glorious deeds that
Would give them a name that live on to this day
"Come on, you sons of bitches, do you wanna live forever?"




Second to none, a marine and a gun
Raising hell as they're fighting like dogs of war
Heart of the corps and a part of the lore
The deadliest weapon on Earth


Kill, fight, die
That's what a soldier should do
Top of their game, earning their name
They were the devil dogs
In a war machine, they were the USA
And since then, they are the devil dogs of war
And then always are the USA marines