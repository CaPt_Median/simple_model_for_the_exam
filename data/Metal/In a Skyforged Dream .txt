Another day through this kingdom of lies
Another  fight to survive and try to make it
Can  you be free?
Out of the storm through the end of the night
There is no way to return, the bridge is burning
Can  you be free?

This  world will leave us deaf and blind
So tonight‚ we'll spread our wings
The  time has come to leave the past behind
And fulfill our destiny
Walk out of the shadows for eternity
In a dream‚ we will rise
Through  the skies made of fire
Burning forever and ever
And we'll reach for the stars
Flying higher and higher
Unbound until the end

I know the pain you felt since you were born
But never again should you feel all alone
You will be free
Don't be afraid‚ there is no need to hide
Together forever, side by side
We will be free

This world has left us deaf and blind
So tonight, we'll spread our wings
The time has come to leave the past behind
And fulfill our destiny
Walk out of the shadows for eternity

In a dream‚ we will rise
Through the skies made of fire
Burning forever and ever
And we'll reach for the stars
Flying higher and higher
Unbound until the end
When your hopes are dying
Don't be afraid of falling
And keep on fighting till the end

In a dream, we will rise
Through the skies made of fire
Burning forever and ever
And we'll reach for the stars
Flying higher and higher
Unbound until the end

In a dream, we will rise
Through the skies made of fire
Burning forever and ever
And we'll reach for the stars
Flying higher and higher
Unbound until the end