
Why do I see her
Through never-ending nights?
Why do I see her
Wearing nothing but the dark?


Have you come here to warn me
Of what I cannot see?
You want to tell me something
But you do not have the words

I know where you live
I can see through your darkness
And when you sleep
I hear the heart that beats you


Have you come here to warn me
Of what I cannot see?
You want to tell me something
But you do not have the words


Eyes far into the distance
A life that does not connect
Time played out its part
On the strings that bind us


Encounters in silence
Words elude the fading night
Wish I could fathom
What is too hard to tell


Her head hangs low
In the silence of her room
Her head hangs low
She takes a bite out of her heart

Have you come here to warn me
Of what I cannot see?
You want to tell me something
But you do not have the words