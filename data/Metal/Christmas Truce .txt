
Silence
Oh, I remember the silence
On a cold winter day
After many months on the battlefield
And we were used to the violence
Then all the cannons went silent
And the snow fell
Voices sang to me from no man's land


We are all
We are all
We are all
We are all friends

And today we're all brothers
Tonight we're all friеnds
A moment of peace in a war that nеver ends
Today we're all brothers
We drink and unite
Now Christmas has arrived and the snow turns the ground white


Hear carols from the trenches
We sing O Holy Night
Our guns laid to rest among snowflakes
A Christmas in the trenches
A Christmas on the front far from home


Madness
(Madness)
Oh, I remember the sadness
(Sadness)
We were hiding our tears
(Hiding our tears)
In a foreign land where we faced our fears
(Faced our fears)
We were soldiers
(Soldiers)
Carried the war on our shoulders
(Shoulders)
For our nations
(Nations)
Is that why we bury our friends?
(Bury our friends)

We were all
We were all
We were all
We were all friends
(Yes, we were friends)


And today we're all brothers
Tonight we're all friends
A moment of peace in a war that never ends
Today we're all brothers
We drink and unite
Now Christmas has arrived and the snow turns the ground white


Hear carols from the trenches
We sing O Holy Night
Our guns laid to rest among snowflakes
A Christmas in the trenches
A Christmas on the front far from home


We were all
We were all
We were all
We were all friends
(Yes, we were friends)

And today we're all brothers
Tonight we're all friends
A moment of peace in a war that never ends
Today we're all brothers
We drink and unite
Now Christmas has arrived and the snow turns the ground white


A Christmas on the frontline
We walk among our friends
We don't think about tomorrow
The battle will commence
When we celebrated Christmas
We thought about our friends
Those who never made it home
When the battle had commenced