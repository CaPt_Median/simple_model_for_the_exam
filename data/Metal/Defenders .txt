
Life's uncertain, a rat-race of pain
Endlessly searching for more
Hopeless and needless, held back by the chains
'Til we can't take anymore

Hold on, stay strong
Breaking out from the past life fading
No more, what for?
All we got is to keep on praying
Pray for the done for their lives in dismay
Witness the final decay
Suffering in silence for years we were blind
But now we got something to say


Hold on, stay strong
'Til the end, not afraid of dying
No more, what for?
Nothing left but to keep on fighting


United, incited, not misguided
We'll fight through time before our one true destiny
Benighted, ignited, still strong and undivided
This day foretold now here for all to see


So free in the blue skies with time marches on
Once more maybe we'll find our peace to behold
Strangers in a strange world, defenders through time
Cleansed in a kingdom of fire


Legions of darkness from beyond the grave
Spill their blood under the sun
Voices are calling with anger and rage
The final war now has begun
Hold on, stay strong
'Til the end, not afraid of dying
No more, what for?
Nothing left but to keep on fighting


United, incited, not misguided
We'll fight through time before our one true destiny
Benighted, ignited, still strong and undivided
This day foretold now here for all to see


So free in the blue skies with time marches on
Once more maybe we'll find our peace to behold
Strangers in a strange world, defenders through time
Cleansed in a kingdom of unholy fire
Hear our call, save us all
Standing tall tonight


The flame will slowly fade away
The shadows of our lives
We wash away the memories
Before the last sunrise

We'll keep holding on
In times of changes
Standing strong against the wind
Until the end of time



Hold on, stay strong
'Til the end, not afraid of dying
No more, what for?
Nothing left but to keep on fighting


United, incited, not misguided
We'll fight through time before our one true destiny
Benighted, ignited, still strong and undivided
This day foretold now here for all to see


So free in the blue skies with time marches on
Once more maybe we'll find our peace to behold
Strangers in a strange world, defenders through time
Cleansed in a kingdom of unholy fire
Hear our call, save us all
Standing tall tonight
Hear our call, save us all
We're standing tall tonight