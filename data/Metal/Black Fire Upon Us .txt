
Tonight, we ride on clouds of fire
We're damned by gods, our deaths conspired
We fear no mortals in this world
The gift we give you is your soul

Fly with us, tonight
Fly with us, tonight


The sky will break
Black fire will wake

Fly on, through the night
We built an alliance, our numbers are strong
We gather, but we don't pray to gods
"What fools, what lunatics", they must think of us

On this night, we will journey
Far, beyond this world
And you must know
That we will never come back home, again


But now we must fly, beasts in the night
Tragic in the sky, battlefield in sight
Storm gathers strong, cold blackened flame
Tell us our future, stories of the slain

Fire grows strong, freezing our skin
Vision is clouded, the rain will begin!


Dangerous creatures, those that opposed us
Raped all their power, bartered with warlocks
Cheated the demons for ancient spells
The blackened fire waits to consume us

So now, we'll say goodbye
Because tonight, we die
So, we say goodbye
So, we'll say goodbye

We die!


Tonight, we ride on clouds of fire
We're damned by gods, our deaths conspired
We fear no mortals in these worlds
The gift we give you is your soul