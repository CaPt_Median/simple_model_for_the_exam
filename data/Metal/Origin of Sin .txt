
Fury
Hell hath no fury
Mercy
I need your mercy
Shadow work incomplete
Abundant worry, cease to bleed
You are mine
In your mind so constantly
Inescapably
Sweetest child, don't you see?
The veil is so thin
Soon all will see, life will see
We have been deceived

But as we're born anew
The water birth, we tried and true
Blackness spawned, now end's consumed
Man's wisdom will surpass doom
I know we struggle here
The ways of old will die again
Some of us will lie with them
To end the cyclе of endless fear (Fеar, fear, fear)
You'll soon remember why you’re here


And so it was written and spoken to few
Empty brotherhoods from tainted truth
From the Egyptian times to the modern rule
We asked for truth, but were only fooled


Don't cast your stones at me
Because you simply cannot perceive
What you've been subjected to
We must question all or we are doomed (Doomed, doomed)


But as we’re born anew
The water birth, we tried and true
Blackness spawned, now end's consumed
Man's wisdom will surpass doom
I know we struggle here
The ways of old will die again
Some of us will lie with them
To end the cycle of endless fear (Fear)

Your spirit is attainable, but locked within you
Between the bone, blood and the sinew
This forever journey only continues
Reaching deep within, remember what you once knew


And so it was written and spoken to few
Empty brotherhoods from tainted truth
From the Egyptian times to the modern rule
We asked for truth, but were only fooled


Don't cast your stones at me
Because you simply cannot perceive
What you've been subjected to
We must question all or we are doomed


And so it was written and spoken to few
Empty brotherhood from tainted truth
From the Egyptian times to modern rule
We asked for truth, but were only fooled


Think or die
Die
Die
Think or die
Think or die