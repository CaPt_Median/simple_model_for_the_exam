Burning fires
Burning lives
On the long distant roads
Through the lost mountains endless
So far away from home

Crossing battles
Savage seas towards the mountains high
Forest plains of wilderness
We're striking out tonight
On towards our destiny
We travel far and wide
Journey through the darkness
As your hearts refuse to die
In the flames of hell
We fire at will the fires of doom has come
With the forces of the blackest knights
They're staring at the sun
Far across the distant plains of ice
We're searching for the sword
When the time has come for battle now
We follow with the horde
We will ride with fire burning hot towards the night sky
In the land of long ago
Forever in our souls
Fly on wings of shining steel are burning so bright
In ancient lands of warriors we're riding on again
Burning fires
Burning lives
On the long distant roads
Through the lost mountains endless so far away from home
Warrior soldiers forever
We fought long ago
We're all lost in the darkness
So far away from home
Fallen soldiers taste the steel of death
The daylight dawning
Sun will shine upon the lives of burning hearts of ice
As you break through the boundaries of life this feeling of despair
And they die in their sleep for the world that will not care
You feel lost in this labyrinth of pain
This sickening dismay
There's a voice inside that's calling
Another wasted day
Can't you see the history
The suffocating madness?
In the land of fallen souls
There's nowhere left, no place to go
I have traveled far and wide across this wasteland
Still searching for the answers for the right to understand
Burning fires
Burning lives
On the long distant roads
Through the lost mountains endless so far away from home
Warrior soldiers forever we fought long ago
We're all lost in the darkness
So far away from home

Riding through the starlight
And smashing the boundaries
As hellfire falls from the sky
A shadow of pain will arise from the ashes
Of those fallen ones who have died
Our only master with fire
And fury of hell will see his bidding done
Blasting from high as the battle unfolds
To the gates of the city we come

We will ride with fire burning hot
Towards the night sky
In the land of long ago forever in our souls
Fly on wings of shining steel are burning so bright
In ancient lands of warriors
We're riding on again
Burning fires
Burning lives
On the long distant roads
Through the lost mountains endless
So far away from home
Warrior soldiers forever we fought long ago
We're all lost in the darkness
So far away from home