Cantus lupus
Satura luna
Corpus nudus domina
Agnus totus
Animus mortus
Ave deus sinistra
Cultus lupus
Opus damnatus
Metus mortis nocturna
Terra sanguis
Padre occultus
Sanctus pupus anima

Dark is the day
God of the grey
Born to obey
All they will say
Mine is the way
Fall down and pray

Hey, hey, wolves don't pray!

Symbols of pain
Out in the rain
Sent to remain
Harvest of grain
Mine is the blame
Curse of the sane

Hey, hey, wolves don't pray!

Ahh - Lupus dei

Pater noster, qui es in caelis:
Sanctificetur nomen tuum
Adveniat regnum tuum
Fiat voluntas tua, sicut in caelo, et in terra
Panem nostrum cotidianum da nobis hodie
Et dimitte nobis debita nostra
Sicut et nos dimittimus debitoribus nostris
Et ne nos inducas in tentationem
Sed libera nos a malo
Quia tuum est regnum et potestas et gloria
In saecula
Amen