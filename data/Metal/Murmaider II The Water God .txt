
The gods watch over you
And they consider what you've done

But now you've hidden away to gain your strength
Deep in a cave, your power is regained
And your legacy has spread to the deepest of the sea
The one that brought them vengeance, that fights for his belief

He can swim through water, he has poisoned teeth
His tentacles have murdered, his scream can kill the weak
He's got explosives? Check! Corrosives? Check!
A master at the art of murder
Mermaids weep the blackened tears
So you swim to a sunken ship
Invited by a soul who bleeds from the lips
The prophet who beckons you, wades in the dark
Speaks and ancient language, this language is of sharks

He says "You're the one that I decree
The one that can save us and set us free
You've gained the power of a deity
You have the strength to wake us from this sleep"


And so, you swim on
And swim on
And so, you swim on
And swim on
Keep swimming, now


And the prophet gives to you, this warning
If this path is chosen, you'll be met with strife
For you'll be seen as the enemy of the sacred
And the gods will curse you for the rest of your life

And you say "I get by just fine (Check)
I've known much worse life (Check)
I've conquered dark times (Check)
They should fear my mind" (Check)
So now, you know then go then
Swim on through the cold then
Harness your strength
Because one day, you may be called

To meet the mighty gods
Deep, within the ocean
And if you're not prepared
Your soul will not be spared


Your eyes have gone black, you'll never look back
You'll never stop swimming, you'll always be tracked
Your life is transformed, your power has grown
Your minions stretch for leagues for a bloodied coral throne

A crown of murdered foes will sit upon your head
Those that wish to challenge you will wish that they were dead
The beasts of the sea will collect and submit
Pray for your forgiveness and live as you permit


And you'll say "I am the water god
You will bow to the water god
You will live for the water god
And you will die for the water god"
Live for the water god
Die for the water god


And the deities loom nearer
(They will find you...)