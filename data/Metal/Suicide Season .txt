
We stare at broken clocks, the hands don't turn anymore
The days turn into nights, empty hearts and empty places
The day you lost him, I slowly lost you too
For when he died, he took a part of you


No time for farewells, no chances for goodbyes
No explanations, no fucking reasons why
I watched it eat you up, pieces falling on the floor
We stare at broken clocks, the hands don't turn anymore

If sorrow could build a staircase or tears could show the way
I would climb my way to heaven and bring him back home again
Don't give up hope, my friend, this is not the end


We stare at broken clocks, the hands don't turn anymore
The days turn into nights, empty hearts and empty places
The day you lost him (Lost him), I slowly lost you too
For when he died, he took a part of you


Death is only a chapter, so let's rip out the pages of yesterday
Death is only a horizon, and I'm ready for the sun
I'm ready for the sun to, I'm ready for the sun to set
This is Suicide Season


If only sorrow could build a staircase or tears could show the way
I would climb my way up to heaven to bring him back home again
If only sorrow could build a staircase or tears could show the way
I would climb my way up to heaven and bring him back home again
If sorrow could build a staircase or tears could show the way
I would climb my way to heaven to bring him back home
This is Suicide Season
This is Suicide Season, my friend
If sorrow could build a staircase or tears could show the way
I would climb my way to heaven to bring him home again
If only sorrow could build a staircase or tears could show the way
I would climb my way to heaven and bring him home again
If only sorrow

If only sorrow could build a staircase, our tears would show the way
We would climb our way to heaven and bring him home again
If sorrow could build a staircase or tears could show the way
We would climb our way to heaven and bring him home again


If only sorrow could build a staircase or tears could show the way
We would climb our way to heaven and bring him home again
We would do (Suicide) anything (Suicide) to bring him back to you (Suicide Season)
We would do anything to end (Suicide) what you're going through (Suicide)
If only sorrow could build a staircase or tears could show the way
I would climb my way to heaven and bring him home again
I would do anything to bring him back to you
Because if you got him back, I would get back the friend that I once knew