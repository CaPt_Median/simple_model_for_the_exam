
Drifting alone, upon the Dragon ship
Eerily still, no winds that tear or whip
Silence deafens, the sea is mirror calm
Blood runs like the water from my palm


I'm standing by myself
Abiding
Just me and no one else
No hiding

Horizon crumbles under flaming skies
A shadow screams from thunderclouds up high
Nidhögg comes in winds of burning flames
Cast down, war and endless pain


It's coming; brace myself
I'm ready
It's coming; no escape
Stand Steady


Out on this bloody sea
I'll face my destiny
Lost in a dreadful dream
I'll meet my fate

Out on this bloody sea
This is my prophecy
A notion of what's to be
There's no escape


The dragon sweeps down with a roar
Sky and ocean shake
It tears up waves of blood and gore
The longship nearly breaks
Passing just above my head
A stench of putrid death
Rotting flesh of thousands dead
Dwells upon it's breath


I turn around, face myself
Yet, it isn't I
With fear and rage I run me through
As I watch me die!
Watch me die!


Out on this bloody sea
I'll face my destiny
Lost in a dreadful dream
I'll meet my fate

Out on this bloody sea
This is my prophecy
A notion of what's to be
There's no escape


Get ready!
Stand steady!
It's coming, brace myself
Abiding
It's coming, certain death
No hiding