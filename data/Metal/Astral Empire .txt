Hear the cries of war
At the break of dawn
See the evil spread disease and fear to all

Through the centuries
Blood shall be the key
Now a leader of our destiny

As his army rose
Crushing endless foes
From the battle he will rain fires of hell from the sky
Bringer of the fire
For the great empire
Rising power of the fallen warriors' souls

Steel crushing steel, see the great fire descend from the sky
Never surrender, one day we'll be free
Flee from the swarm, through the forces of terror
No sacrifice 'til the end

Fly far away through the raging storm
'Til the end with the rise of the sun
On the winds rising higher through our astral empire
Break the chains, now this war shall be won

Hunting down their prey
From beyond the grave
Crush them into dust, a life of worthless slaves

Soon the world will see
Evil prophecy
Now we shall avenge a thousand warriors' souls

Now dark knowledge is mine
Voices inside
Witness the fall for the last time
Black is the power
Now is the hour to decide
Once more to

Fly far away through the raging storm
'Til the end with the rise of the sun
On the winds rising higher through our astral empire
Break the chains, now this war shall be won

Now dark knowledge is mine
Voices inside
Witness the fall for the last time

Black is the power
Now is the hour to decide
Once more to

Fly far away through the raging storm
'Til the end with the rise of the sun
On the winds rising higher through our astral empire
Break the chains, now this war shall be won