Can you imagine that it's all up to you?
Evil embrace of powers unnatural
You're left in silence nothing more to behold
You close your eyes and visualize the day you'll get away

It's here, it's near
I see it all so clear
Tonight your flight will take you higher
It's here, it's near
You live a life of fear
Tonight is the night that will inspire
Crying alone at night your destiny unknown
Your every being full of rage and hate
Seems like you're running but you just can not hide
It's in a raging fear all solitude is left behind

It's here, it's near
I see it all so clear
Tonight your flight will take you higher
It's here, it's near
You live a life of fear
Tonight is the night that will inspire

Can he comfort thee?
And fill your life with ecstasy
Through the darkness
And the pain
Will you be the ones again?
Disciples of Babylon

Lead: Vadim

It's here, it's near
I see it all so clear
Tonight your flight will take you higher
It's here, it's near
You live a life of fear
Tonight is the night that will inspire
Can he comfort thee?
And fill your life with ecstasy
Through the darkness
And the pain
Will you be the ones again?
Disciples of Babylon

Lead: Herman
Acoustic Break
Solos: Sam/Herman/Herman

It's here, it's near
I see it all so clear
Tonight your flight will take you higher
It's here, it's near
You live a life of fear
Tonight is the night that will inspire

Can he comfort thee?
And fill your life with ecstasy
Through the darkness
And the pain
Will you be the ones again?
Disciples of Babylon