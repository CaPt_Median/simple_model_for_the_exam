Deus Rex Cobra

I pray to Heaven that we don’t wake the Cobra tonight
And I say a prayer when we call for the blood of the wild
This time in the venom of the Cobra lies the might
Deus in serpens nominum, all we die

Wo-ho, this is the kiss of the Cobra King
Wo-ho, this is the kiss of the Cobra
Wo-ho, this is the kiss of the Cobra King
Wo-ho, this is the kiss of the Cobra
I pray to Heaven we escape from the poison in veins
And I bring a sacrifice before the light of dawn inhumane
This night, when in a bite of the Cobra Christ's alive
Deus in serpens nominum, all we die

Wo-ho, this is the kiss of the Cobra King
Wo-ho, this is the kiss of the Cobra
Wo-ho, this is the kiss of the Cobra King
Wo-ho, this is the kiss of the Cobra

Deus Rex Cobra
Beware the kiss of the Cobra

Wo-ho, resist the kiss of the Cobra King
Wo-ho, resist the kiss of the Cobra
(Cobra, Cobra) Wo-ho (Cobra, Cobra)
(Cobra, Cobra) Beware the kiss of the Cobra King (Cobra, Cobra)
(Cobra, Cobra) Wo-ho (Cobra, Cobra)