
I journey through this frozen land on my own
There's no shelter anywhere down the road
When this mystic wanderer comes around
And the dying hope in my heart starts to grow

The dying hope in my heart starts to grow
Starts to grow


This vagrant offers help when there was none
To this outlawed murderer on the run
Through the snowy mountains we roam
My journey towards freedom has now begun

On the run
All alone
Hope is gone
Wanderer


A blizzard hit with blinding force
The wind cut through the bone
And amidst the blur of snow
I found myself alone

Freezing winds brought me down
I laid myself to rest
In the deep, chilling cold
I knew I'd freeze to death

Left alone I had no choice
But to accept my fate
When the wanderer returned
Then I knew that my life was saved


On the run
All alone
Hope's returned
Wanderer
On the run
Not alone
The road goes on
Wanderer


I'm haunted by dreams of blood
The visions of death and gore
It is the future that I see
And if this is truly my fate
Then so be it