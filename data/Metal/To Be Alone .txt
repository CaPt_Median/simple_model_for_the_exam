
Hey you, can you tell me when enough is enough?
When you've pushed enough buttons and it's time to back up?
Well, would you know when someone else has had enough of you?
(Of you, of you, yes, you)
Just take a second, you should listen to yourself
Everybody is to blame but no one else is around
Is any of it sinking in and making sense to you?
(Of you, yes, you, it's true)


Is there no means to an end?

So tell me, how does it feel to know no one is coming?
No one is running when you hit the ground (You hit the ground)
How does it feel to know that no one's around you?
No one will be there when you hit the ground (You hit the ground)
How does it feel?
How does it feel to be alone?


Well, now that I've got you here and you're all alone
Is there anyone you need to call before we move on?
Well, is there anybody out there thinking of you?
(It's true, of you, yes, you)
I can see the empty look in your eyes
Like someone came along, stole your heart, then left you to die, yeah
I know the feeling 'cause you did the same things to me
(Of you, it's true, it's true)


Is there a means to the end?


So tell me, how does it feel to know no one is coming?
No one is running when you hit the ground (You hit the ground)
How does it feel to know that no one's around you?
No one will be there when you hit the ground (You hit the ground)
How does it feel?
How does it feel to be alone?

To be alone


So tell me, how does it feel to know no one is coming?
No one is running when you hit the ground (You hit the ground)
How does it feel to know that no one's around you?
No one will be there when you hit the ground (You hit the ground)
How does it feel?
How does it feel to be alone?


To be alone
To be alone
To be alone