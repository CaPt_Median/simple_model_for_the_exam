
Someone left the door open
Who left me outside?
I'm bent, I'm not broken
Come live in my life
All the words left unspoken
Are the pages I write
On my knees and I'm hoping
That someone holds me tonight
Hold me tonight

Welcome to the world and all the land in it was wasted
The blood upon your hands and the wickedness that made it
Sing or scream it all and the memories keep fading
See the exit wound, dear God, what have we taken?
Guess I'll say a prayer and I'll kiss into the air
I'll look into the sky, send 'em straight to nowhere
We all dug the grave, can't shake away the shame
Can't quiver in the sky but you're shaken all the same
You left us with the guns and all of 'em unloaded
Teach us how to shoot, but you taught us how to hold 'em
And all the weight of all the world is right between your shoulders
'Cause heavy is the heart when the world keeps growing colder


Who left the door open?
Who left me outside?
I'm on my knees and I'm hoping
That someone holds me tonight
Hold me tonight


Two sides to every story, here's a little morning glory
Breakfast off a mirror, dying slow seems a little boring
Burning like a flag, walking straight into the breeze
'Cause there's two types of people, you are weak or you are me, yeah
What's another lifetime like mine?
We all die a little sometimes, it's alright
Did you come to say your goodbyes to this life?
We all hurt a little sometimes, we're alright
So mothers hold your children, don’t you ever let 'em go
There is weakness in your grip and they are holding all the hopes
Don't you ever let me go, don't you ever let me go
Don't you ever let me go, don't you ever let me go

Who left the door open?
Who left me outside?
I'm on my knees and I'm hoping
That someone holds me tonight
Hold me tonight


Someone left the door open
Who left me outside?
I'm bent, I'm not broken
Come live in my life
All the words left unspoken
Are the pages I write
On my knees and I'm hoping
That someone holds me tonight
Hold me tonight