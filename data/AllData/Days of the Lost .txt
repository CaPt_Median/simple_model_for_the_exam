
To lean on the impossible
Is the failure of our dreams
The days of independence
Need revolutions to reveal


The fact, the word, the undenied
Take a shot at the lies
The way, the eyes, the nondescript
Give in to muted relief

Fall to the form
Fit into the norm


Now I see you standing all by yourself
There in the silence
I see you're all to yourself
And thеse are the hours
Thеse are the days
These are the hours
These are the days of the lost


In the end what stands to reason
Like pillars to uphold
Must be a universal
Indivisible line


Fall to the form
Fit into the norm


Now I see you standing all by yourself
There in the silence
I see you’re all to yourself
These are the hours
These are the days
These are the hours
These are the days of the lost



Now I see you standing all by yourself
There in the silence
I see you're all to yourself
These are the hours
Now I see you standing all by yourself
There in the silence
Now I see you standing all by yourself
These are the hours
These are the days
These are the hours
Days of the lost