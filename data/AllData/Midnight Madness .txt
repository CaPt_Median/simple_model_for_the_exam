March on, the fire in the sky
Live now, tomorrow we may die
Stand strong, desire to survive
Till the end of time

No more, you'll never walk alone
Starlight will guide us to our home
Reborn, the sands of time are sown
Till we claim our throne

Aggravation, termination
Generations in starvation
Mass salvation of the nation
Now!
We will fly tonight towards the angels
See a star shine bright into the sky
We will stand tonight and live forever
We're the masters of the stars and of all time

We will fly tonight towards the angels
Wee a star shine bright into the sky
And we will stand tonight and live forever
We're the masters of the stars and of all time

Dark clouds, the symphony of war
Stare now into the crystal ball
Stay strong and face a winter storm
Till the final call

Strike now, the power from within
Hunt down, and crush them for their sins
This time the battle we will win
So it now begins

Suffocation, desperation
Close temptation to predation
Blind destruction of creation
Now!

We will fly tonight towards the angels
See a star shine bright into the sky
We will stand tonight and live forever
We're the masters of the stars and of all time
We will fly tonight towards the angels
See a star shine bright into the sky
And we will stand tonight and live forever
We're the masters of the stars and of all time

Once lost but now we're free
To the night we hold the key
Silently we wait the last command

Guardians of space and time
To the mountain we must climb
At the gates of glory we now stand
For the final stand!



Whoa whoa whoa whoa whoa

We will fly tonight towards the angels
See a star shine bright into the sky
We will stand tonight and live forever
We're masters of the stars and of all time

We will fly tonight towards the angels
See a star shine bright into the sky
And we will stand tonight and live forever
We're masters of the stars and of all time
And on we ride!