
This is not a failure, this is not an end
These are just symptoms of a trajectory descent
Further down the line you hide from the memories of unresolve
The struggle is lost


All this time of knowing
Letting piece by piece slip away
'Cause when they're all against you
You fail

Don't you force my hand
Don't you force me
Don't you force my hand


When the silence has spoken in his imminent storm
Taken to the nth degree where failure forms


Spiralling deception
Left the mindless drifting away
Cause when they're out to get you
You fail


Don't you force my hand
Don't you force me
Don't you force my hand


All this time ends in nothing
These walls of doubt built into our minds
All this time signifying something
A victim without circumstance

All this time of knowing
Letting piece by piece slip away
'Cause when they're all against you
You fail


Don't you force my hand
Don't you force me
Don't you force my hand