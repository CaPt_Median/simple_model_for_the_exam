
This matters more with time
Than anything I know
This object permanence
Keeps you with me 'til the end
Inconsequential I may be I know
Inside the silence


In truth divided
I have lost myself again
When words are not enough
And time is not the healer
What is gone inside
Has left me here without

This hardened heart resists
I'm left here barely standing
What lies behind the fear
Belies the grand perspective
When I don't want to see it
And I don't want to hear
The world is silent


In truth divided
I have lost myself again
When words are not enough
And time is not the healer
What is gone inside
Has left me here without


Inside the fire
I cannot see
Inside
I cannot find you


In truth divided
I have lost myself again
When words are not enough
And time is not the healer
What is gone inside
Has left me here without

In truth divided
I don't believe
I have lost myself again
This search within
This search within
Has gone inside
And left without