
We spent the last of our time
In isolation
Without a question to the night
Must we go, to the end
Must we fall behind the lines
We must go, for ourselves, to the end


Inside the turmoil of the mind
The truth lies further away
From the light of the day to the blind decisions
True contradiction

I don't want to be
Where you think I am
Self-deception is a part of me
What is always there for you
A truth worth lying for


We turned hours into days
Meaning into nothing
Without a moment to recall
Must we take what we have to remember
We must make of ourselves what we lack


Outside the instinct there is light
Our time is ending
We take control, we hold the line
Against this blinding fear


I don't want to be
Where you think I am
Self-deception is a part of me
What is always there for you
A truth worth lying for
The one who's never there is a part of me

I don't want to be
Where you think I am
Self-deception is a part of me
What is always there for you
A truth worth lying for
The one who's never there


I don't want to be
Where you think I am
Self-deception is a part of me
What is always there for you
A truth worth lying for
The one who's never there is a part of me