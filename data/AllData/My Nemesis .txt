
I'm leaving behind this world
And all the things I am (The things I am)
I'm tearing away from it
Because I know I can
I'm pushing away from you
And all the things you are (The things you are)
Don't need the memory
I already wear the scars


There is no yesterday (Yesterday)
Tomorrow's far away

I (I) gave you everything
And in return, you gave me nothing
Show me a sign (Sign)
Please give me anything
I will not hide from what's inside of me
My nemesis


I'm turning away from me
And all the things I've known (The things I've known)
I don't need your help no more
I can do this on my own
I'm taking all the blame
Resent what I've become (What I've become)
I regret everything and
There's nowhere left to run


There is no yesterday (Yesterday)
Tomorrow's far away


I (I) gave you everything
And in return, you gave me nothing
Show me a sign (Sign)
Please give me anything
I will not hide from what's inside of me
My nemesis



I (I) gave you everything
And in return, you gave me nothing
Show me a sign (Sign)
Please give me anything
I will not hide from what's inside of me
My nemesis