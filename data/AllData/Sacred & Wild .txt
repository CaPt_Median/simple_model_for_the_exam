
In the dark of the night we are demons in silence
In the light of the moon we are the storm of the damned
In the heat of the wild we are the bloodred horizon
Stand anywhere we land


In the call of the wild we are the thunder and lightning
In the roar of the fight we are the sword in your rear
In the heart of the night we are the call of the sirens
Near anytime you fear, anytime you fear

We are the dark of the night
We are the sermon of fight
We bring the nightside sacred and wild

We are the demons of light
The holy word you can't fight
We bring the nightside sacred and wild

Fight, fight, sacred and wild


In the light of the morning we are preachers and tyrants
By the break of the dawn we are the dark of the land
By the first of the sunlight we are strong as the bible
Stand anywhere we land


When the dark of the night has come we stand up as wild men
When the land of the living dies we rise from the dead
When the last of the sun has gone we leave or exile
We head sanctify the dead, sanctify the dead


We are the dark of the night
We are the sermon of fight
We bring the nightside sacred and wild
We are the demons of light
The holy word you can't fight
We bring the nightside sacred and wild

Fight, fight, sacred and wild
Fight, fight, sacred and wild

Sunctus iesus
Sacred and wild
Dius pater
Sacred and wild

We are the dark of the night
We are the sermon of fight
We bring the nightside sacred and wild

We are the demons of light
The holy word you can't fight
We bring the nightside sacred and wild

Fight, fight, sacred and wild