
At a certain point it happens: life becomes to real
And nothing can hold back what is about to be
I don't know how close we need to get
To blind out the surroundings and focus on the real


I know you're gone but this reality is crashing down on me
Crashing down to defeat all purpose
I know you're gone
I'm just not ready, I'm just not done
I am ever in resistant disbelief

When raw emotion gets in the way of our thoughts and words
We pass so far beyond our breaking point
I don't know how far we need to be
From all that we don't want to see to regain perspective


I know you're gone but this reality is crashing down on me
Crashing down to defeat all purpose
I know you're gone
I'm just not ready, I'm just not done
I am ever in resistant disbelief


I know yet I fail to find purpose and form
I know but the truth just makes it harder


I know you're gone but this reality is crashing down on me
Crashing down to defeat all purpose
I know you're gone
I'm just not ready, I'm just not done
I am ever in resistant disbelief