
In the realm of Svartalvheim
Where master forgers reign
Loki met with Eitri and Brokk
With malice and deceit, he got them to agree
To create nine magics gifts for the Asa gods


Brokkr had a sense of foul play in the air
So he made a wager for Loke's head

Treasures will be forged for the Asa gods
A spear and ring for the Asgard king
But finest of them all, The Crusher it is called
Mjölner, hammer of Thor


Loki's treachery knows no boundaries
He hid himself in the blacksmith's cave
But as work progressed, he feared he'd lose his bet
He knew his situation now was grave
Working the bellows
Heating the forge
Striking the anvil
Striking with force
Then as they worked on the last gift
A mighty hammer of war
Loki disrupted the work of the blacksmiths
The handle came out short


The nine gifts were brought to Odin's mighty hall
As Loki feared the Gods praised them all


Treasures have been forged for the Asa gods
For the Vana prince, a ship and boar
But finest of them all, The Crusher it is called
Mjölner, hammer of Thor

Brokkr came to claim his price
Loki fooled him twice
He saved his lying head
But got his mouth sewn shut instead


Treasures have been forged for the Asa gods
For Sif they made new hair of gold
But finest of them all, The Crusher it is called
Mjölner, hammer of Thor