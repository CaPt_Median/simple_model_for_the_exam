
Force it to begin, spark the flame
To firebursting sin, doctrine is denied
Over and over again
Keeping death of all things private
Expose this failed creation
What is done is dead, what is done is dead
To be content, an abomination
Are you satisfied, are you done with this?


The ultimate rebellion
The sacrifice is endless
The ultimate rebellion
Be nothing to no one

What's in a true life behind the crumbling walls?
Of dream and infant illusion
Action without movement, desire without heart
It is over and done, it is over and done
To crave for touchless objects and fathom without eyes
The fall of all things personal that no one sees


The ultimate rebellion
The sacrifice is endless
The ultimate rebellion
Leave nothing to no one




Awaken to the screams of unresolved
Demands and accusations, words are free
These are the outer symptoms of inner detonation
That will force us to inaction, rid us of resolve
Take the burden off me, lead me to my grave


The ultimate rebellion
The sacrifice is endless
The ultimate rebellion
Leave nothing to no one