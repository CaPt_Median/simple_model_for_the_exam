
We are of the persuasion
We are of the cause
That fail within our systems
We are the nothingness
Uphold the courtesy
While taking the crown


Rise from the inside
Rise from the norm
We are the blindness
We are the blind

Gateways to the fear among you
Lifelong race to leave behind
Deep inside the violence rages
Till the fear within you dies


Gateways to the fear among you
Lifelong race to leave behind
Deep inside the violence rages
Till the fear within you dies


It takes a moment
It extracts moments out from time
Where you alter the course
That leads to the end of denial
And do you give up?
Or do you give in?


Gateways to the fear among you
Lifelong race to leave behind
Deep inside the violence rages
Till the fear within you dies

Gateways to the fear among you
Lifelong race to leave behind
Deep inside the violence rages
Till the fear within you dies


Do you give up?
Or do you give in?
Do you give up?
Or do you give in?


Gateways to the fear among you
Lifelong race to leave behind
Deep inside the violence rages
Till the fear within you dies


Gateways to the fear among you
(Gateway to the fear)
Race to leave behind
(Do you give in?)
This violence rages
Till the fear (Till the fear, till the fear) within you dies