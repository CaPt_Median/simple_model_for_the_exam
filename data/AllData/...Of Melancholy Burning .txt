Jackal, aches for pain beyond me
Bestiality beckons - The anger set free
For there is no pain greater than thine
For there is no gain but the fury inside

Desolated since derived
Torn screaming from the gaping wound

Always be cherished
The grandeur of melancholy
Outward reprisal
Swear by your throne
Fallen words shall grieve thee
The grandeur of melancholy

Frailty, thy name is weakness
Vengeance, thy name is pain
Storm through the still glowing night
Ember eyes beyond reason shall see

Flee from the safety of the sheltering sky
See all but logic, so vengeance shall be
Mine is the grandeur of melancholy burning...
Oh burning

Charge into uncertainty's promised land

Always and never
Your are the nail

Cursed, cursed
Oh essence of the night guide me
Cursed, cursed
Oh sweet revenge heal me
Frailty, thy name is weakness
Vengeance, thy name is pain
The nail
Jackal, aches for pain beyond me
The storm that now grabs me
Is the storm of my soul
For there is no pain greater than mine
For there is no gain but the fury inside

Once so bereaved
And ever so suppressed
Charge and split up the anger
Wake up the jackal
Let out his wrath

Always and never
He spoke of his pain
Always and never
You are the nail

Always be cherished
The grandeur of melancholy
Always and never
You are the nail