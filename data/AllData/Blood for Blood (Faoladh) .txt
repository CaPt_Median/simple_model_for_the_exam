
Stand up from the dead in victorious sight
We strike with the ire of God
Come stand up for war, no surrender at night
All riders are drowned in their blood
And evangelists gone in the flood


To the end, to defend
To the glorious night
To the praise of the cross and the dead

Fight us, spite us
We have the might and the glory
Bite us, light us
Strike with the force of a God
Swear us, tear us
We are the fall of the holy
All the night, it's blood for blood


Altars to defend and Christ in command
We die for the Lord and the cross
Onward to the end, we all wander this land
No combat we fear and no loss
No surrender no battle is lost


To the end, to repent
To the glorious times
To the praise of the fallen and damned


Fight us, spite us
We have the might and the glory
Bite us, light us
Strike with the force of a God
Swear us, tear us
We are the fall of the holy
All the night, it's blood for blood

Fallen to descent, we died in command
No prayer, no crest, and no throne
Sunken and banned, no rest, no repent
The bravest of hearts turned to stone


Faster, wider
We come alive under fire
Darker, wilder
Stricken by fever at heart
Sacred riders
Born into flame on the pyre
Hear the call, it's blood for blood