
From the reign I've built
Of shame and guilt
I go back and take a look
At myself and what
I have done so far
At the bridge he's lying
Waiting 'til light


No choice to go
Back where, he was
Okay to us
You're to come
And save me

Going down I walk
My way 'til now
Deep to the obscurity
Obliterating you in every way
With my life's insanity


Battered up again
I'm going right inside
Curled up on the floor
Wonder ways to kill the pain and
What doesn't kill you
Will only make you pissed off


Sweep back to kill
You can't make it go away
So let's take a shot at me
Just when you punch me, I bleed


Going down I walk
My way 'til now
Deep to the obscurity
Obliterating you in every way
With my life's insanity

No choice to go
Back where, he was
Okay to us
You're to come
And save me


Going down I walk
My way 'til now
Deep to the obscurity
Obliterating you in every way
With my life's insanity