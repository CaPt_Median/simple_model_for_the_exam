
The unsaid tone of weak despair
Fail to resonate
Frayed end of our binding thread
Will disintegrate
By the laws our physics stage
Failure to communicate


None too sentient
Hear no, see no worthless magic
Against all function
Ongoing choices the trials will end
Filter the nonsense
And laugh at what's left
Indecision/nonvision
What matters taken away

Look at the shell that is you
Empty, fragile, weak
Soon the battle is over
Lost to apathy


So overcome with pointless tears
To test pain receptors
Nothing matters ever here
Put up a non-reaction
These eyes will never see
Covered up from reality


Look at the shell that is you
Empty, fragile, weak
Soon the battle is over
Lost to apathy


The unknown world that you deny
No priority
Cannot fail if you never start
How predictable
I want to know where for madness to start
Always the sceptic and never be part
Introvertive, indescriptive
It matters not, not

Look at the shell that is you
Empty, fragile, and weak


Look at the shell that is you
Empty, fragile, weak
Soon the battle is over
Lost to apathy


The shell that is you
Empty, fragile, weak
Oh, the battle is over
Lost to apathy