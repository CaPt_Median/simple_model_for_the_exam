
We're worried about echoes
And why we're being held back
Bound at the speed of light


This is the time of no regret
In a forward momentum race
We leave in our wake
What is broken, forgotten and gone

I squint my eyes
As if to make my sight
Extend a little further


This is the time of no regret
In a forward momentum race
We leave in our wake
What is broken and forgotten


The dream is nothing
Yet it drives us to the end
These words unforce our hand
Hold on, hold on


In fear of true deception
Our lives are being brought back
Brought back down


This is the time of no regret
In a forward momentum race
We leave in our wake
What is broken and forgotten