
In the flicker of a light
In a shadow on the wall
In the reassuring voice
Of apophenia, my friend
I hear signals in the static
I see distant figures calling
And I know it's not to be
Yet let there be revelation


I see movement in every colour
I see patterns, I see forms
There is a way to feel affirmed
A way to know for certain
Where answers to our questions
Remain in the unknown
There is a way to feel
A way to know
Where meaning and logic
Remain in the unknown

Apophenia, my friend
In ripples across the water
Our seething minds embrace
The false positive of comfort
To register as nothing
Nothing but willed illusion
I know just what this is
But please, let it be a revelation


I see movement in every colour
I see patterns, I see forms
There is a way to feel affirmed
A way to know for certain
Where answers to our questions
Remain in the unknown
There is a way to feel
A way to know
Where meaning and logic
Remain in the unknown


With nothing to break the illusion
To break the spell

Just knowing what this is not
Is enough to dispel the doubting
And give into delusion
Let it be revelation
It gives purpose to the mundane


I see movement in every colour
I see patterns, I see forms
There is a way to feel affirmed
A way to know for certain
Where answers to our questions
Remain in the unknown
There is a way to feel
A way to know
Where meaning and logic
Remain in the unknown