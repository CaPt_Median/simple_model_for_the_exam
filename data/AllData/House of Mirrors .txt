
See the choice, it lies before you
A simple flick of the wrist
Blank doors keep the world at bay
Or do they keep the outside in?
What geometry lies beyond?
A familiar façade, keys in locks


Unhinge the future
Shut out the past
Winners and losers
Fiction and fact

Renegade are reflections made across a sharpened blade
In a house of mirrors you’re never alone
Welcome home!


To open is to invitе the unknown
In to flood your once square homе
The walls silently watch you
Are you watching them back?
Windows to the souls surround you
What escapes when those panes crack?


Unhinge the future
Shut out the past
Winners and losers
Fiction and fact


Renegade are reflections made across a sharpened blade
In a house of mirrors you’re never alone
Welcome home!
Suspicious simulacra distort your flesh and bone
In a house of mirrors you’re never alone
Welcome home!

You’ll run
You’ll hide
Or try to
But we all
Reside
Inside you




Renegade are reflections made across a sharpened blade
In a house of mirrors you’re never alone
Welcome home!
Suspicious simulacra distort your flesh and bone
In a house of mirrors you’re never alone
Welcome home!