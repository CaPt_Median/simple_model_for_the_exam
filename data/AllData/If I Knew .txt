Loneliness seems like an evil dream
Reality is life and life is here
Once my heart was ruled by emptiness
Harmed because of being too sincere

But now that you've crossed my way
Could it be we found it again

If I knew that I'm your only one
I could live and make it through time
But if one day You just be gone
I wouldn't know how to ever get by... oooh yeah...
How can I make you feel secure
We're both the same, I'm king and you are queen
Understand you make me breathe again
Tell me all your thoughts and I will see

Take my hands and we'll be there
Together someday somehow

If I knew that I'm your only one
I could live and make it throught time
But if one day
You just be gone
I wouldn't know how to ever get by
I wouldn't know how to ever get by... oohh...

If I knew that I'm your only one
I could live and make it throught time
But if one day
You just be gone
I wouldn't know how to ever get by
I wouldn't know how to ever get by
I wouldn't know how to ever get by
I would simply break down and cry...