
Barbarus, Iratus
Midnight Madonna
Barbarus, Iratus
Midnight Madonna


In the final night bring sundown forever
In the darkest time we follow the trail of this game
In the fullmoon light the godless endeavor
Stand pale and lunar

On a dead end sky so restless but ever
Giving up the fight, we bring back the blood in your name
And again tonight
Let out a beast you can't tame


Come raise your hands and fight
Get alive and wake your might
Midnight Madonna come
Get alive and break the light
Midnight Madonna


In the primal dawn, bring blood from the living
On the sabbath morn, we strike back and leave no farewell
And a foreign might, held strong by the father's hand
Sanguine order


On a sabbath night, so faithless but ever
Living up defied, we bring back the saints of despair
And before the dawn
Sacred and borne on the air


Come raise your hands and fight
Get alive and wake your might
Midnight Madonna come
Get alive and break the light
Midnight Madonna

Come raise your hands and fight
Get alive and wake your might
Midnight Madonna come
Get alive and break the light
Midnight Madonna