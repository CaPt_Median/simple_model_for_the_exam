Ny tid nalkas, denna tid går mot sitt slut
Hela Stockholm se mig krönas
Kanoner skjut salut
Ingen ed avlagd, ingen ed jag svär
Kronan kommer ej från kyrkan
Den kom direkt ifrån Gud
Bevisat min styrka genom strid
Född att regera, att föra krig
Knä om Knä

Min tid är här
Över Norden jag härskar
Med det arv som jag gavs
Gång på gång
Sjung Carolus sång
Krigets konst jag behärskar
Låt mitt namn sprida skräck
Gång på gång
Sjung Carolus sång
Än en gång, sjung Carolus sång

Född att härska, leda mina män i strid
Ingen man kan mig befalla
Jag lyder under Gud
Hör min order, ifrågasätt mig ej
Vet att sådan är min vilja, och därvid skall det ske
Sida vid sida uti strid, med karoliner går jag i krig, knä om knä

Min tid är här

Över Norden jag härskar
Med det arv som jag gavs
Gång på gång
Sjung Carolus sång
Krigets konst jag behärskar
Låt mitt namn sprida skräck
Gång på gång
Sjung Carolus sång
Än en gång, sjung Carolus sång
Allt jag ser, vill ha mer, vem skall stoppa mig?
Hela Europa ska böja sig för min armé
Vad är ditt, skall bli mitt, då jag dräper dig
Min vilja ske

Bevisat min styrka genom strid
Född att regera, att föra krig
Knä om knä

Min vilja ske

Över Norden jag härskar
Med det arv som jag gavs
Gång på gång
Sjung Carolus sång
Än en gång
Krigets konst jag behärskar
Låt mitt namn sprida skräck
Gång på gång
Sjung Carolus sång
Än en gång, sjung Carolus sång