
See the wolf in sheep's skin
Waiting in the shadows
Watching, learning
Too bad I did not recognize


The dark undertow
Pulling you away
We've been infiltrated, in fact, violated
Carefully orchestrated

Hate springs eternal
In this black heart of mine
Time heals nothing
I'll never forgive
Hate springs eternal
In this black heart of mine
Time heals nothing
I'll never forgive


You swallowed the lies
In a search for inner peace
With open ears and eyes
Could you not feel


The dark undertow
Pulling you down
You've been infiltrated, in fact, violated
Ultimately desecrated


Hate springs eternal
In this black heart of mine
Time heals nothing
I'll never forgive
Hate springs eternal
In this black heart of mine
Time heals nothing
I'll never forgive
Never forget
Never

When the dust has settled
I'll watch you drown, drown
Drown in your regret


The dark undertow
Pulling you away
We've been infiltrated, in fact, violated
Carefully orchestrated


Hate springs eternal
In this black heart of mine
Time heals nothing
I'll never forgive
Hate springs eternal
In this black heart of mine
Time heals nothing
I'll never forgive
Never forget
Never, ever