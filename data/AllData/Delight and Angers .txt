
Everyday takes figuring out how to live
Sometimes it feels like a mistake
Sometimes it's a winners parade
Delight and angers
I guess that's the way it's supposed to be


Please heal me, I can't sleep
Thought I was unbreakable, but this is killing me
Call me, everything
Make me feel unbreakable, lie and set me free

I feel the fear takes hold
Afraid this hell I create is my own
Calm my franticness, I can't take it anymore
This used to be my own world, but now I've lost control


Please heal me, I can't sleep
Thought I was unbreakable, but this is killing me
Call me, everything
Make me feel unbreakable, lie and set me free


Chasing leftovers
Under the fading sun
Searching for shelter
I feel my time has come




Please heal me, I can't sleep
Thought I was unbreakable, but this is killing me
Call me, everything
Make me feel unbreakable, lie and set me free