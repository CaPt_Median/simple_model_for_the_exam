import argparse
import NGramModel


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--model', help='path to the file with the model')
    parser.add_argument('--prefix', help='beginning of the sentence you want to generate')
    parser.add_argument('--length', help='length of the generated sentence', type=int)
    args = parser.parse_args()
    model = NGramModel.NGramModel(args.model)
    model.load()
    if args.prefix:
        print(model.generate(args.length, args.prefix))
    else:
        print(model.generate(args.length))


if __name__ == '__main__':
    main()